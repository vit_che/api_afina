<?php

namespace MainBundle\Controller;

//use http\Env\Request;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use MainBundle\Entity\User;
use MainBundle\Entity\Role;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use FOS\RestBundle\Controller\Annotations as Rest;

class UserController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);
    }


    /**
     * @Rest\Get("/users")
     * @Method("GET")
     */
    public function allUsersAction()
    {
        $users = $this->getDoctrine()
            ->getRepository('MainBundle:User')
            ->findAll();
        $jsonContent = $this->serializer->serialize($users, 'json');
        $arr = json_decode($jsonContent, true);

        return $this->json($arr);
    }

    /**
     * @Rest\Get("/users/{id}")
     * @Method("GET")
     */
    public function getUserAction($id)
    {
        $user = $this->getDoctrine()
            ->getRepository('MainBundle:User')
            ->find($id);
        if ($user) {
            $jsonContent = $this->serializer->serialize($user, 'json');
            $arr = json_decode($jsonContent, true);

            return $this->json($arr);

        } else {

            return $this->json(["message" => "There is not user with this id"]);
        }
    }

    /**
     * @Rest\Put("/users/{id}")
     */
    public function editUserAction(Request $request, $id)
    {
        if ($request->getMethod() == "PUT" && $request->getContentType() == "json" && $request->getContent() != "" ){

            $res = json_decode($request->getContent(), true);

            $user = $this->getDoctrine()
                    ->getRepository('MainBundle:User')
                    ->find($id);

            if ( $res['name'] ){
                $user->setName($res["name"]);
            }
            if ( $res['email'] ){
                $user->setEmail($res["email"]);
            }
            if ( $res['role'] ){
                $id = $res['role'];
                $role = $this->getDoctrine()
                    ->getRepository('MainBundle:Role')
                    ->find($id);
                $user->setRole($role);
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->json(["message" => "User was updated"]);
        }
    }


    /**
     * @Rest\Post("/users")
     */
    public function createUserAction(Request $request)
    {
        if ($request->getMethod() == "POST" && $request->getContentType() == "json" && $request->getContent() != "" ){

            $res = json_decode($request->getContent(), true);

            $user = new User();
            if ( $res['name'] && $res['email'] && $res['role'] ){

                $user->setName($res["name"]);
                $user->setEmail($res["email"]);

                $id = $res['role'];
                $role = $this->getDoctrine()
                    ->getRepository('MainBundle:Role')
                    ->find($id);
                $user->setRole($role);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->json(["message" => "User was created"]);
            }
        }
    }

    /**
     * @Rest\Delete("/users/{id}")
     */
    public function deleteUserAction(Request $request, $id)
    {
        if ($request->getMethod() == "DELETE"){

            $user = $this->getDoctrine()
                ->getRepository('MainBundle:User')
                ->find($id);

            if($user){

                $em = $this->getDoctrine()->getManager();
                $em->remove($user);
                $em->flush();

                return $this->json(["message" => "User was deleted"]);
            }
        }
    }

    /**
     * @Rest\Get("/users/roles/{id}")
     * @Method("GET")
     */
    public function roleUsersListAction($id)
    {
        $users = $this->get('doctrine')
            ->getRepository('MainBundle:User')
            ->createQueryBuilder('u')
            ->where('u.role=:id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
        $jsonContent = $this->serializer->serialize($users, 'json');
        $arr = json_decode($jsonContent, true);

        return $this->json($arr);
    }

}

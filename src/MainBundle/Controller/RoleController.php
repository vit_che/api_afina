<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use MainBundle\Entity\User;
use MainBundle\Entity\Role;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

use FOS\RestBundle\Controller\Annotations as Rest;

class RoleController extends Controller
{
    private $serializer;

    public function __construct()
    {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $this->serializer = new Serializer($normalizers, $encoders);
    }

    /**
     * @Rest\Get("/roles")
     * @Method("GET")
     */
    public function allRolesAction()
    {
        $roles = $this->getDoctrine()
            ->getRepository('MainBundle:Role')
            ->findAll();
        $jsonContent = $this->serializer->serialize($roles, 'json');
        $arr = json_decode($jsonContent, true);

        return $this->json($arr);
    }

    /**
     * @Rest\Get("/roles/{id}")
     * @Method("GET")
     */
    public function getRoleAction($id)
    {
        $role = $this->getDoctrine()
            ->getRepository('MainBundle:Role')
            ->find($id);
        $jsonContent = $this->serializer->serialize($role, 'json');
        $arr = json_decode($jsonContent, true);

        return $this->json($arr);
    }

    /**
     * @Rest\Put("/roles/{id}")
     */
    public function editRoleAction(Request $request, $id)
    {
        if ($request->getMethod() == "PUT" && $request->getContentType() == "json" && $request->getContent() != "" ){

            $res = json_decode($request->getContent(), true);

            $role = $this->getDoctrine()
                ->getRepository('MainBundle:Role')
                ->find($id);

            if ( $res['name'] ){
                $role->setName($res["name"]);
            }

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->json(["message" => "Role was updated"]);
        }
    }

    /**
     * @Rest\Post("/roles")
     */
    public function createRoleAction(Request $request)
    {
        if ($request->getMethod() == "POST" && $request->getContentType() == "json" && $request->getContent() != "" ){

            $res = json_decode($request->getContent(), true);

            $role = new Role();
            if ( $res['name'] ){
                $role->setName($res["name"]);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush();

            return $this->json(["message" => "Role was created"]);
        }
    }

    /**
     * @Rest\Delete("/roles/{id}")
     */
    public function deleteRoleAction(Request $request, $id)
    {
        if ($request->getMethod() == "DELETE"){

            $role = $this->getDoctrine()
                ->getRepository('MainBundle:Role')
                ->find($id);

            if ($role){
                $users = $this->get('doctrine')
                    ->getRepository('MainBundle:User')
                    ->createQueryBuilder('u')
                    ->where('u.role='.$id)
                    ->getQuery()
                    ->getResult();

                if (count($users)){

                    return $this->json(["message" => "The Role has users! please delete they before"]);
                }

                $em = $this->getDoctrine()->getManager();
                $em->remove($role);
                $em->flush();

                return $this->json(["message" => "Role was deleted"]);
            }
        }

        return $this->json(["message" => "Role wasn't deleted"]);
    }
}

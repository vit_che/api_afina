TEST TASK 

=======
- Create two applications using Symfony3 framework:
1. server with API entries
2. client

- Server stores users and groups in MySQL database.
user table: id, name, email
group table: id, name

- Client accessing the server through the API:
1. should be able to add, edit, delete users and groups on the server (well, CRUD)
2. should be able to get report with the list of users of each group.

The ready made project should be able to start using Vagrant. 
Assessment will be based on both code and project running within Vagrant.




AFINA  REST API 


get Users             GET  /users
get one user          GET  /users/{user_id}
create user           POST /users
edit user             PUT  /users/{user_id}
remove user           DELETE /users/{user_id}
get same role users   GET /users/roles/{role_id}

get Roles             GET /roles
get Role              GET /roles/{role_id}
create Role           POST /roles
edit Role             PUT /roles/{role_id}
remove Role           DELETE /roles/{role_id}

